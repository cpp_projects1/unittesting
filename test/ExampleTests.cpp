/*
    Reference: https://www.youtube.com/watch?v=Lp1ifh9TuFI&ab_channel=Rhymu%27sVideos
    GoogleTest git repo: https://github.com/google/googletest.git

    Extensions required for Visual Studio Code:
        CMAKE TOOLS
        C/C++ for Visual Studio Code
        Copiler? Maybe?
    
    Notes:
        Remeber to change 
        gtest_force_shared_crt:BOOL=OFF -> gtest_force_shared_crt:BOOL=ON

    New location of .exe
    build\test\Debug\ExampleTests.exe

    To select specific tests:
    build\test\Debug\ExampleTests.exe --gtest_filter=ExampleTests_F*
    build\test\Debug\ExampleTests.exe --gtest_filter=ExampleTests_T*
    build\test\Debug\ExampleTests.exe --gtest_filter=*MAC
*/

#include "../Example.hpp"

#include <gtest/gtest.h>

struct ExampleTests_F : public::testing::Test
{
    int* x;
    int GetX(){
        return *x;
    }
    virtual void SetUp() override{
        fprintf(stderr, "Start!\n");
        x = new int(42);
    }
    virtual void TearDown() override {
        fprintf(stderr, "Tear Down!\n");
        delete x;
    }
};

//It's illegal to mix Test and Test Suits!
TEST(ExampleTests_T, DemonstrateGTestMacros){
    EXPECT_TRUE(true);
    EXPECT_EQ(true,true) << "Hello!";  //Only prints when test Failed
}

TEST(ExampleTests_T, MAC){
    int x = 42;
    int y = 16;
    int sum = 100;
    int oldSum = sum;
    int expectedNewSum = oldSum + x * y;
    EXPECT_EQ(expectedNewSum, MAC(x, y, sum));
    EXPECT_EQ(expectedNewSum, sum);
}

TEST(ExampleTests_T, Square){
    int x = 5;
    int expectedSquare = x * x;
    EXPECT_EQ(expectedSquare, Square(x));
}

TEST_F(ExampleTests_F, MAC_F){
    int y = 16;
    int sum = 100;
    int oldSum = sum;
    int expectedNewSum = oldSum + GetX() * y;
    EXPECT_EQ(expectedNewSum, MAC(GetX(), y, sum));
    EXPECT_EQ(expectedNewSum, sum);
}

TEST_F(ExampleTests_F, Square_F){
    int expectedSquare = GetX() * GetX();
    EXPECT_EQ(expectedSquare, Square(GetX()));
}